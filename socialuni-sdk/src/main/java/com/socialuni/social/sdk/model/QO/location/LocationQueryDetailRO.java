package com.socialuni.social.sdk.model.QO.location;

import lombok.Data;

@Data
public class LocationQueryDetailRO {
    //纬度
    private Double lat;
    //经度
    private Double lng;
}
