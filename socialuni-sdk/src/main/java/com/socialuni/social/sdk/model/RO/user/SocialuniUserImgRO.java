package com.socialuni.social.sdk.model.RO.user;


import lombok.Data;

@Data
public class SocialuniUserImgRO {
    private String id;
    private String src;
    private Double aspectRatio;
}
