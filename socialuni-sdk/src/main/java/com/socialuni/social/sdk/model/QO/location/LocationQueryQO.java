package com.socialuni.social.sdk.model.QO.location;

import lombok.Data;

@Data
public class LocationQueryQO {
    //ip
    private String ip;
    //纬度
    private String latitude;
    //经度
    private String longitude;
}
