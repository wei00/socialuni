package com.socialuni.social.sdk.dao.repository;

import com.socialuni.social.sdk.dao.DO.keywords.KeywordsCopyDO;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author qinkaiyuan
 * @date 2018-10-17 21:59
 */
public interface KeywordsCopyRepository extends JpaRepository<KeywordsCopyDO, Integer> {

}


