package com.socialuni.social.common.exception.exception;

public class UniSdkException extends RuntimeException {
    public UniSdkException(String message) {
        super(message);
    }
}
