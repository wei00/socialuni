package com.socialuni.social.tance.sdk.api;

public interface SocialRequestUserConfig {
    String getToken();

    Integer getUserId();
}